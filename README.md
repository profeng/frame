# generator-frame [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url] [![Coverage percentage][coveralls-image]][coveralls-url]
> vue2.0 前端框架

## Installation

First, install [Yeoman](http://yeoman.io) and generator-frame using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
npm install -g generator-frame
```

Then generate your new project:

```bash
yo frame
```

## Getting To Know Yeoman

 * Yeoman has a heart of gold.
 * Yeoman is a person with feelings and opinions, but is very easy to work with.
 * Yeoman can be too opinionated at times but is easily convinced not to be.
 * Feel free to [learn more about Yeoman](http://yeoman.io/).

## License

Apache-2.0 © [徐峰]()


[npm-image]: https://badge.fury.io/js/generator-frame.svg
[npm-url]: https://npmjs.org/package/generator-frame
[travis-image]: https://travis-ci.org/xufeng/generator-frame.svg?branch=master
[travis-url]: https://travis-ci.org/xufeng/generator-frame
[daviddm-image]: https://david-dm.org/xufeng/generator-frame.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/xufeng/generator-frame
[coveralls-image]: https://coveralls.io/repos/xufeng/generator-frame/badge.svg
[coveralls-url]: https://coveralls.io/r/xufeng/generator-frame
